"use strict";

$(document).ready(function () {

    /**
     * Parese JSON response and build string of HTML table
     * @param countries
     * @return string
     */
    function buildRow(countries) {
        let builtTable = '';

        countries.forEach(function (country) {
            builtTable += '<div class="div-table-row">';
            builtTable += `<div class="div-table-col">${country.zone}</div>`;
            builtTable += `<div class="div-table-col">${country.name}(${country.native_name})</div>`;
            builtTable += `<div class="div-table-col">${country.language}(${country.native_language})</div>`;
            builtTable += `<div class="div-table-col">${country.currency}(${country.currency_code})</div>`;
            builtTable += `<div class="div-table-col">${country.latitude}</div>`;
            builtTable += `<div class="div-table-col">${country.longitude}</div>`;
            builtTable += '</div>';
        });

        return builtTable
    }

    $.ajax(
        'data.php',
        {
            success: function (data) {
                const selectedBody = $("#table-body");
                selectedBody.empty();
                const responseData = JSON.parse(data);
                let tableBody = '';
                for (const zones in responseData) {
                    let currentZone = responseData[zones];
                    tableBody += buildRow(currentZone);
                }
                selectedBody.append(tableBody);
            },
            error: function () {
                console.log('There was some error performing the AJAX call!');
            }
        }
    );

    //Post for the countries using EUR as currency
    $.post("data.php", {"currency": "Euro"}, function (response) {
        const selectedBody = $("#currency-table-body");
        selectedBody.empty();
        const responseData = JSON.parse(response);
        let tableBody = '';
        for (const zones in responseData) {
            let currentZone = responseData[zones];
            currentZone.forEach(function (country) {
                tableBody += '<tr>';
                tableBody += `<th scope="row">${country.zone}</th>`;
                tableBody += `<td>${country.name}(${country.native_name})</td>`;
                tableBody += `<td>${country.language}(${country.native_language})</td>`;
                tableBody += `<td>${country.currency}(${country.currency_code})</td>`;
                tableBody += `<td>${country.latitude}</td>`;
                tableBody += `<td>${country.longitude}</td>`;
                tableBody += '</tr>';
            });
        }
        selectedBody.append(tableBody);
    }).fail(function () {
        console.log("Error could not fetch countries");
    })

    $(document).on('change', '#zones', function () {
        const selectedZone = $(this).val();
        $.post("data.php", {"zone": selectedZone}, function (response) {
            const selectedBody = $("#table-body");
            selectedBody.empty();
            const responseData = JSON.parse(response);
            let tableBody = '';
            for (const zones in responseData) {
                let currentZone = responseData[zones];
                tableBody += buildRow(currentZone);
            }
            selectedBody.append(tableBody);
        }).fail(function () {
            console.log("Error could not fetch countries according to zone : " + selectedZone);
        })
    });

});

// <tr>
// <th scope="row">1</th>
//     <td>Mark</td>
//     <td>Otto</td>
//     <td>@mdo</td>
//     <td>@mdo</td>
//     <td>@mdo</td>
//</tr>

// {
//     "zone": "central",
//     "name": "Germany",
//     "native_name": "Deutschland",
//     "language": "Germany",
//     "native_language": "Deutsch",
//     "currency": "Euro",
//     "currency_code": "EUR",
//     "map_url": "https:\/\/www.google.ro\/maps\/place\/Germany\/@51.1641175,10.4541194,6z\/data=!3m1!4b1!4m2!3m1!1s0x479a721ec2b1be6b:0x75e85d6b8e91e55b?hl=en",
//     "latitude": "51.1641175",
//     "longitude": "10.4541194"
// },