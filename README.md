# iPsos-test

Se dă codul XML din fișierul countries.xml. Se cer următoarele:

**1.** codul XML este intenționat invalid, se cere să fie corectat.

**2.** să se scrie un script PHP care să citească conținutul fișierului XML, apoi să afișeze toate informațiile din el într-un tabel, ordonate după regiune și numele în engleză al țărilor, ca de exemplu:

| **Regiune** | **Țară** | **Limbă** | **Monedă** | **Latitudine** | **Longitudine** |
| --- | --- | --- | --- | --- | --- |
| central | Germany (_Deutschland_) | Germanian (_Deutsch_) | Euro (_EUR_) | 51.1641175 | 10.4541194 |
| central | Italy (_Italia_) | Italian (_Italiano_) | Euro (_EUR_) | 41.2924601 | 12.5736108 |
| eastern | Bulgaria (_България_) | Bulgarian (_български_) | Лев (_BGN_) | 42.7253401 | 25.4833039 |

etc.

* Afisarea tabelului se va face folosind div-uri si nu tag-ul \&lt;table\&gt;.

* Notă: informația din coloanele Latitudine și Longitudine trebuie să fie extrase din nodul map\_url folosind Regex. De exemplu, pentru România, cele două informații sunt cu bold în link:

* https://www.google.ro/maps/place/Romania/@ **45.9419466** , **25.0094305** ,7z/data=!3m1!4b1!4m2!3m1!1s0x40b1ff26958976c3:0x84ef4f92a804b194?hl=en

* latitudinea: 45.9419466

* longitudinea: 25.0094305

**3.** În același script, folosind sintaxa XPath, să se extragă și să se afișeze sub tabelul de mai sus numele țărilor care au moneda &quot;Euro&quot;.

**4.** Sa se adauge un filtru in header-ul tabelului, pe coloana Regiune. Filtrul va fi de tip dropdown si va contine valorile: choose(valoarea default), western, central si eastern. In functie de valoarea selectata in dropdown, se vor afisa doar tarile care contin respectivul atribut (ex. Pentru eastern afisam Romania, Bulgaria si Greece).