<?php


class XMLSanitize
{
    /**
     * @var string
     */
    const SCHEMA_DEFINITION = "../SchemasXSD/schema-country.xsd";

    /**
     * @var string
     */
    const XML_FOLDER = 'test-info/';

    /**
     * @var string
     */
    const LAT_LONG_REGEX = "/(@(\-?\d{1,2})\.\d+\,(\-?\d{1,2}\.\d+\,))/";

    /**
     * @var bool
     */
    public bool $parse_ready = false;

    /**
     * @var string
     */
    public string $valid_xml_file = '';


    /**
     * XMLSanitize constructor.
     * @param string $file_name
     * @throws Exception
     */
    public function __construct(string $file_name)
    {
        if (file_exists(self::XML_FOLDER . $file_name)) {
            $this->validateXML($file_name);
        } else {
            echo "<h1 style='text-align: center;color: red'>XML File provided cannot be found!!</h1>";
            throw new Exception('XML File provided cannot be found');
        }

    }

    /**
     * @param string $file_name
     */
    private function validateXML(string $file_name)
    {
        $xml = new DOMDocument();
        $xml->load($file_name);
        if (!$xml->schemaValidate(self::SCHEMA_DEFINITION)) {
            // we need to fix the XML and then write it to a new file
            $this->fixBrokenXML(self::XML_FOLDER . $file_name);
        } else {
            $this->valid_xml_file = $file_name;
            echo "<p>This is a valid XML File<p/>";
        }
    }


    /**
     * Fix the malformed XML and save the valid one into
     * a new file
     *
     * @param string $file_name
     * @return bool
     */
    private function fixBrokenXML(string $file_name)
    {
        $recovery_status = false;

        try {
            // we need to fix the XML and then write it to a new file
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file_name);
            $valid_filename = "{$withoutExt}-valid.xml";

            $xml = new DOMDocument();
            $xml->recover = true;
            $xml->validateOnParse = true;
            $xml->load($file_name);
            $xml->save("{$valid_filename}");

            if (!$xml->schemaValidate(self::SCHEMA_DEFINITION)) {
                $this->correctNesting($valid_filename);
            }

            $recovery_status = true;
            $this->parse_ready = $recovery_status;
            $this->valid_xml_file = $valid_filename;

            return $recovery_status;
        } catch (Exception $e) {
            echo 'Failed to recover the XML structure: ', $e->getMessage(), "\n";
        }

        return $recovery_status;
    }

    /**
     * Correcting XML for incorrect fixing of previous library
     * @param string $filename
     * @return mixed
     */
    private function correctNesting(string $filename)
    {
        $existingXML = simplexml_load_file($filename);
        return $this->recursivelyIterate($existingXML, $filename);
    }

    /**
     * @param SimpleXMLElement $elements
     * @param string $filename
     * @return mixed
     */
    private function recursivelyIterate(SimpleXMLElement $elements, string $filename)
    {
        $parsedJSON = [];

        foreach ($elements as $element) {
            if (!property_exists($element, 'country')) {
                $coordinates = $this->extractLatLong($element->map_url);
                array_push($parsedJSON,
                    self::setCountryInfo(
                        $element->attributes()->zone,
                        $element->name,
                        $element->name->attributes()->native,
                        $element->language,
                        $element->language->attributes()->native,
                        $element->currency,
                        $element->currency->attributes()->code,
                        $element->map_url,
                        $coordinates[0],
                        $coordinates[1]
                    ));
            } else if (property_exists($element, 'country') && count($element->country)) {
                if (count($element->country)) {
                    for ($x = 0; $x < count($element->country); $x++) {
                        $coordinates = $this->extractLatLong($element->country[$x]->map_url);
                        array_push($parsedJSON,
                            self::setCountryInfo(
                                $element->country[$x]->attributes()->zone,
                                $element->country[$x]->name,
                                $element->country[$x]->name->attributes()->native,
                                $element->country[$x]->language,
                                $element->country[$x]->language->attributes()->native,
                                $element->country[$x]->currency,
                                $element->country[$x]->currency->attributes()->code,
                                $element->country[$x]->map_url,
                                $coordinates[0],
                                $coordinates[1]
                            ));
                    }
                    //Fetch the element itself and clean it from unwanted children
                    $coordinates = $this->extractLatLong($element->map_url);
                    array_push($parsedJSON,
                        self::setCountryInfo(
                            $element->attributes()->zone,
                            $element->name,
                            $element->name->attributes()->native,
                            $element->language,
                            $element->language->attributes()->native,
                            $element->currency,
                            $element->currency->attributes()->code,
                            $element->map_url,
                            $coordinates[0],
                            $coordinates[1]
                        ));
                }
            } // Write a line to call the function itself again make it recursive
        }

        $this->rebuildXMLToStandard($parsedJSON, $filename);

        file_put_contents(self::XML_FOLDER . "countries.json", json_encode($parsedJSON, JSON_PRETTY_PRINT));
    }

    /**
     * @param array $countriesData
     * @param string $filename
     */
    private function rebuildXMLToStandard(array $countriesData, string $filename)
    {
        /**
         * Building a new and valid XML that will respect the XSD
         */
        $xml = new XMLWriter();
        $xml->openUri($filename);
        $xml->setIndent(true);
        $xml->startDocument('1.0', 'utf-8');
        $xml->startElement('countries');

        foreach ($countriesData as $country) {

            $xml->startElement('country');
            $xml->writeAttribute('zone', $country['zone']);

            $xml->startElement('name');
            $xml->writeAttribute('native', $country['native_name']);
            $xml->text($country['name']);
            $xml->endElement();

            $xml->startElement('language');
            $xml->writeAttribute('native', $country['native_language']);
            $xml->text($country['name']);
            $xml->endElement();

            $xml->startElement('currency');
            $xml->writeAttribute('code', $country['currency_code']);
            $xml->text($country['currency']);
            $xml->endElement();

            $xml->startElement('map_url');
            $xml->text($country['map_url']);
            $xml->endElement();

            $xml->startElement('latitude');
            $xml->text($country['latitude']);
            $xml->endElement();

            $xml->startElement('longitude');
            $xml->text($country['longitude']);
            $xml->endElement();

            $xml->endElement();
        }

        $xml->endElement();
        $xml->endDocument();
    }

    /**
     * Setting the data for future manipulation with JSON
     *
     * @param string $zone
     * @param string $name
     * @param string $native_name
     * @param string $language
     * @param string $native_language
     * @param string $currency
     * @param string $currency_code
     * @param string $map_url
     * @param string $latitude
     * @param string $longitude
     * @return array
     */
    public static function setCountryInfo(string $zone,
                                    string $name,
                                    string $native_name,
                                    string $language,
                                    string $native_language,
                                    string $currency,
                                    string $currency_code,
                                    string $map_url,
                                    string $latitude,
                                    string $longitude)
    {
        return [
            "zone" => $zone,
            "name" => $name,
            "native_name" => $native_name,
            "language" => $language,
            "native_language" => $native_language,
            "currency" => $currency,
            "currency_code" => $currency_code,
            "map_url" => $map_url,
            "latitude" => $latitude,
            "longitude" => $longitude,
        ];
    }


    /**
     * @param string $mapURL
     * @return array
     */
    private function extractLatLong(string $mapURL)
    {
        preg_match(self::LAT_LONG_REGEX, $mapURL, $matches);
        $found = str_replace("@", "", $matches[0]);
        $found = preg_split('/,/', $found, null, PREG_SPLIT_NO_EMPTY);

        return $found;
    }


}