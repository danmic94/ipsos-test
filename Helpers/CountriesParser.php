<?php


class CountriesParser
{

    /**
     * @var SimpleXMLIterator
     */
    private SimpleXMLIterator $xml_object;


    /**
     * @var string
     */
    private string $sanitized_file = '';

    /**
     * @var array
     */
    public array $parseAndSorted = [];


    /**
     * CountriesParser constructor.
     * @param XMLSanitize $sanitized
     */
    public function __construct(XMLSanitize $sanitized)
    {
        $this->setSanitizedFile($sanitized->valid_xml_file);
    }


    /**
     * @param string $sanitized_file
     */
    public function setSanitizedFile(string $sanitized_file)
    {
        $this->sanitized_file = $sanitized_file;
        $xml_data = file_get_contents($this->sanitized_file);
        $this->xml_object = new SimpleXMLIterator($xml_data);
    }


    /**
     * @param mixed $countries
     */
    public function parseCountries($countries = null)
    {
        if (is_null($countries)) {
            $countries = $this->xml_object;
        }

        foreach ($countries as $country) {
            $currentCountry = XMLSanitize::setCountryInfo(
                $country->attributes()->zone,
                $country->name,
                $country->name->attributes()->native,
                $country->language,
                $country->language->attributes()->native,
                $country->currency,
                $country->currency->attributes()->code,
                $country->map_url,
                $country->latitude,
                $country->longitude
            );

            switch ($country->attributes()->zone) {
                case 'central':
                    $this->parseAndSorted['central'][] = $currentCountry;
                    break;
                case 'eastern':
                    $this->parseAndSorted['eastern'][] = $currentCountry;
                    break;
                case 'western':
                    $this->parseAndSorted['western'][] = $currentCountry;
                    break;
            }
        }

        //Sort zones
        if (count($this->parseAndSorted) > 1) {
            ksort($this->parseAndSorted);
        }
        //Sort country names within zones
        foreach ($this->parseAndSorted as $zone => &$countries_array) {
            usort($countries_array, function ($country_a, $country_b) {
                return strcmp($country_a['name'], $country_b['name']);
            });
        }
    }

    /**
     * @return false|string
     */
    public function resultToJSON()
    {
        echo json_encode($this->parseAndSorted, JSON_PRETTY_PRINT);
    }

    /**
     * @param string $zone
     * @return SimpleXMLElement[]|void
     */
    public function filterByZones(string $zone = '')
    {
        if (empty($zone)) {
            $this->parseCountries($this->xml_object->xpath('/countries/country'));
        } else {
            $this->parseCountries($this->xml_object->xpath('/countries/country[@zone=\'' . $zone . '\']'));
        }
    }

    /**
     * @param string $currency
     * @return SimpleXMLElement[]|void
     */
    public function filterByCurrency(string $currency = '')
    {
        if (empty($currency)) {
            $this->parseCountries($this->xml_object->xpath('/countries/country'));
        } else {
            $this->parseCountries($this->xml_object->xpath('/countries/country[currency=\'' . $currency . '\']'));
        }
    }


}