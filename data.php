<?php

require_once "vendor/autoload.php";

$xml_data = new XMLSanitize('countries.xml');

if ($xml_data->parse_ready) {
    $parse = new CountriesParser($xml_data);
    if (isset($_POST['currency'])) {
        $parse->filterByCurrency($_POST['currency']);
        return $parse->resultToJSON();
    }
    if (isset($_POST['zone']) && $_POST['zone'] !== 'all') {
        $parse->filterByZones($_POST['zone']);
        return $parse->resultToJSON();
    }
    $parse->parseCountries();
    return $parse->resultToJSON();
}

